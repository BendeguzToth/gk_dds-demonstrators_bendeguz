.. (C) ALbert Mietus, dropjes-licentie: copy & use, and betaal met dropjes naar nuttigheid

.. _ZMQ:

================
ZMQ, 0MQ: ZeroMQ
================

   ZeroMQ, ZMQ or 0MQ is a anynchronous messaging library, aimed at use in distributed or concurrent applications.
   The main strength of ZeroMQ comes from the capability of sending and receiving messages in many different ways.
   For example you can have messages between apllications send based on a pub-sub system, request-reply or push-pull system. 
   Within these systems it is then possible to create queues, proxies, pipelines, messages brokers and a whole lot of other components.
   This makes it possible to create many different ways of getting a message from sender to receiver depending on the need of the system. 
          
Notes
=====
The original author “Pieter Hintjens” is *not* a Dutch-man; he was from Zuid-Africa. He is also passed away ...




Install
=======

Python
------

Installing ZMQ for python on linux/raspberry pi can be easily done with:
 
.. code-block:: python

   sudo apt-get install libzmq3 libzmq3-dev python3-zmq

Links
=====
 * http://zeromq.org
