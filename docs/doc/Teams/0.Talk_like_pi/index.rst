



.. figure:: ./img/logo.png
   :align: right
   :scale: 100%



============
Talk like pi
============

   Welcome to the 'talk like pi' team page. 
   To start right away i would recommend going through the setup chapter first so that all requirements are in place to run everything.
   Afterwards go to the running part of each of the protocol/library chapters and it will show how to run the programs.
   
contents
********

.. toctree::
   :maxdepth: 2
   :glob:

   setup
   MQTT
   ZMQ
   DDS
   RoundTrip
   TipsAndTricks
   sprints/index