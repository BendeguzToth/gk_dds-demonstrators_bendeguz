.. _Migrating_Mercurial_to_git:

Migrating mercurial to git
==========================

Question: Can we merge a mercurial repository into existing git repository without losing history?
For the migration of a mercurial to a git repository not much can be found. The are only a few different methods. I found a url where several migration paths to git have been defined (see `url <https://git-scm.com/book/en/v2/Git-and-Other-Systems-Migrating-to-Git>`_).

.. tip::

   Within the following steps repository A will be migrated from a mercurial to a git repository. It is wiste to create a temporary directory in which the mercurial repository will be cloned and migrated to a git repository. 

.. note::

   Most of the steps below are shown from a script used to execute the migration of a mercurial repository to a git repository, see :download:`migrate_mercurial_to_git <./shell-scripts/migrate_mercurial_to_git>`. For the execution of the script the url of the mercurial repository needs to be provided as an argument.

   .. literalinclude:: ./shell-scripts/migrate_mercurial_to_git
      :language: bash
      :lines: 3-6

Steps
-----

Needed tooling for migration:

Clone tooling repository:

.. literalinclude:: ./shell-scripts/migrate_mercurial_to_git
   :language: bash
   :lines: 26-27

.. literalinclude:: ./shell-scripts/migrate_mercurial_to_git
   :language: bash
   :dedent: 3
   :lines: 47-49

.. tip::

     During the first attempts I was getting some errors (unexpected "(" in hg-fast-export.sh, hg-fast-export.py cannot import name revsymbol). Eventually found the resolution to checkout a specific version of the tool.

Mercurial repository:

Clone the mercurial repository into temporary name hg_repo and collect committing users:

.. literalinclude:: ./shell-scripts/migrate_mercurial_to_git
   :language: bash
   :dedent: 3
   :lines: 50-51, 61


Fix the authors file to reflect the correct syntax for users in git:

.. note::
   Example for fixing the authors file:
   |BR|"bob"="Bob Jones <bob@company.com>"
   |BR|"bob@localhost"="Bob Jones <bob@company.com>"
   |BR|"bob <bob@company.com>"="Bob Jones <bob@company.com>"
   |BR|"bob jones <bob <AT> company <DOT> com>"="Bob Jones <bob@company.com>"

Create initial git repository and migrate mercurial repository to this:

.. literalinclude:: ./shell-scripts/migrate_mercurial_to_git
   :language: bash
   :dedent: 3
   :lines: 69-70, 72

.. literalinclude:: ./shell-scripts/migrate_mercurial_to_git
   :language: bash
   :dedent: 6
   :lines: 75

.. note::

   Any next steps depend on what should be the actual result. If this is a new git repository the appropriate action for this need to be executed. In our case it had to be merged into an existing git repository. For that already a procedure has been created (:ref:`Merging Repositories <Merging_Repositories>`). This only needs to be adapted since repository A does not need to be cloned. For repository A the location <tmp_dir>/converted can be used.
