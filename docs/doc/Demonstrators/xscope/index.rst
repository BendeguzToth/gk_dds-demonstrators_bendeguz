####################################################
xScope - A overview of the DDS traffic in one domain
####################################################


************
Introduction
************

Goal of the xScope
=========================

The goal of the xScope is to visualize DDS traffic and learn the basics of DDS.
The xScope is able to subscribe to various 'Topics' and log the data into a .csv file.
The xScope is able to quickly give a graph for first impression at the end of a measurement.
A detailed graph can be made with the .csv file generated.


***************
Getting started
***************

On a Raspberry Pi (linux)
=========================

1. Build and Install Cyclone DDS

Go to the repository of `Cyclone DDS <https://github.com/eclipse-cyclonedds/cyclonedds/>`_.
Follow the installation instructions in README.

.. note:: Sepcifying the DCMAKE_INSTALL_PREFIX path is not required. Directly do *cmake ..* and *cmake --build .*

2. Build and Install Python API of cyclonedds

Go to the repository of `cdds-python <https://github.com/Ashyglim/cdds-python/>`_.
Follow the installation steps.

.. note:: Jsonpickle needs to be installed for python3 (pip3 install jsonpickle)

          Errors may happen while building the Chameleon python API (in 'bit' folder).
          The key is to build the library (ddsstubs.so and ddsc.so) for the python binding.
          Try the following steps if the normal steps do not work.

          Copy the bit folder to the cyclone dds folder under example. Add *addsubdirectory(bit)* to the cmakelist.
          And rebuild cyclone dds. By doing so you could get the library files in the lib folder in */build*.
          Copy this 2 files to */usr/local/lib*.

After installation you can test the API by running

::

          $ python3 test_flexy_writer.py

3. Get the project files

Clone/Download the repository in bitbucket.

4. Install dependencies and Run the xScope

.. note:: For xScope to run the following dependencies need to be installed: numpy, pandas, matplotlib and tkinter

::

          $ pip3 install numpy
          $ pip3 install pandas
          $ pip3 install matplotlib
          $ sudo apt-get install python python3-tk

.. note:: Be aware to set the display settings, since xScope is a gui based application.

::

          $ cd <directory name of repo>/src/demonstrators/XScopy/Python/poc1
          $ ./configure
          $ python3 xScope.py


On Windows
==========


1. Install Visual studio with C/C++ supports

2. Build and install Cyclone DDS using VS compiler

.. warning:: Chameleon cannot be built on windows currently. Try to cross-compile the library file (.dll) on Linux.



*********************
How to run the xScope
*********************

How to run the xScope
=====================

* Make sure the device on which the xScope will be runned is connected on a network where DDS traffic is available.
* Open xscope_config.py and add the 'Topics' from the DDS traffic which you want to see in the xScope.
* Save the file.
* Run the xScope python file in a Terminal (./xScope.py).
* Wait for some DDS traffic.
* When you have enough data close the xScope by pressing ctrl+c.
* The .csv file will be generated in the same folder as the xScope.py file and a graph will be shown.
* Close the graph to close the xScope.
