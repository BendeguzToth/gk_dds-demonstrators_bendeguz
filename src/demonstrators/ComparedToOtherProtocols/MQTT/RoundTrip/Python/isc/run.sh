#!/bin/bash
# roundtrip isc

HOST="192.168.0.202"
HUB="0"
MASTER="1"
BASETOPIC="mqtt/node/"
QOS="0"

Pi0="pi@192.168.0.200"
Pi1="pi@192.168.0.201"
Pi3="pi@192.168.0.203"

trap ctrl_c INT

ctrl_c() {
		echo "knock knock, cleaning lady"
       	sh cleanup.sh
}


# copy MQTT.py the raspberries 
scp ../MQTT.py ${Pi0}:~/MQTT.py
scp ../MQTT.py ${Pi1}:~/MQTT.py
scp	../MQTT.py ${Pi3}:~/MQTT.py


ssh ${Pi0} python3 ~/MQTT.py ${BASETOPIC}1 ${BASETOPIC}2 ${HOST} ${HUB} ${QOS} &
sleep 1

ssh ${Pi1} python3 ~/MQTT.py ${BASETOPIC}2 ${BASETOPIC}3 ${HOST} ${HUB} ${QOS} &
sleep 1

ssh ${Pi3} python3 ~/MQTT.py ${BASETOPIC}3 ${BASETOPIC}4 ${HOST} ${HUB} ${QOS} &
sleep 1

python3 ../MQTT.py ${BASETOPIC}4 ${BASETOPIC}1 ${HOST} ${MASTER} ${QOS}

sh cleanup.sh
